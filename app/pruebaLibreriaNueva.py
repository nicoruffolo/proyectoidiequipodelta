from sentence_transformers import SentenceTransformer, util

# Cargar el modelo preentrenado de SBERT
model = SentenceTransformer('paraphrase-MiniLM-L6-v2')

# Definir las oraciones a comparar
sentence1 = "Esta es la primera oración."
sentence2 = "Esta es la segunda oración."

# Generar las representaciones (embeddings) de las oraciones
embedding1 = model.encode(sentence1, convert_to_tensor=True)
embedding2 = model.encode(sentence2, convert_to_tensor=True)

# Calcular la similitud de coseno entre los embeddings
similarity = util.pytorch_cos_sim(embedding1, embedding2)

print(f"Similitud entre oraciones: {similarity.item()}")