// Datos de los subdiccionarios
var elemento = document.getElementById('miElemento');
var materia =JSON.parse(elemento.getAttribute('data-materia').replace(/'/g, '"')); 
var origen = JSON.parse(elemento.getAttribute('data-origen').replace(/'/g, '"'));
var viaAcceso = JSON.parse(elemento.getAttribute('data-viaAcceso').replace(/'/g, '"'));
var materia_etiquetas = ["Civil y comercial","Contencioso Administrativo / tributario","Familia","Electoral","Previsional","Penal y contravencional","Laboral"]
var materia_datos = [materia['Civil_y_comercial'],materia['Contencioso_Administrativo_tributario'],materia['Familia'],materia['Electoral'],materia['Previsional'],materia['Penal_y_contravencional'],materia['Laboral']]
const datosMateria = {
    data: materia_datos, // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
    // Ahora debería haber tantos background colors como datos
    backgroundColor: [
        'rgba(163,221,203,0.2)',
        'rgba(232,233,161,0.2)',
        'rgba(230,181,102,0.2)',
        'rgba(229,112,126,0.2)',

        'rgba(103,221,203,0.2)',
        'rgba(142,233,161,0.2)',
        'rgba(90,181,102,0.2)',
    ],// Color de fondo
    borderColor: [
        'rgba(163,221,203,1)',
        'rgba(232,233,161,1)',
        'rgba(230,181,102,1)',
        'rgba(229,112,126,1)',

        'rgba(103,221,203,1)',
        'rgba(142,233,161,1)',
        'rgba(90,181,102,1)',
    ],// Color del borde
    borderWidth: 1,// Ancho del borde
};
var origen_etiquetas = ["Federal","Provincial"]
var origen_datos = [origen['Federal'],origen['Provincial']]
console.log(origen)
console.log(origen_datos)
const datosOrigen = {
    data: origen_datos, // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
    // Ahora debería haber tantos background colors como datos
    backgroundColor: [
        'rgba(163,221,203,0.2)',
        'rgba(232,233,161,0.2)',
        
    ],// Color de fondo
    borderColor: [
        'rgba(163,221,203,1)',
        'rgba(232,233,161,1)',
       
    ],// Color del borde
    borderWidth: 1,// Ancho del borde
};
var viaAcceso_etiquetas = ["Originario","Competencia","Competencia apelada de recursos"]
var viaAcceso_datos = [viaAcceso['Originario'],viaAcceso['Competencia'],viaAcceso['Recurso']]
const datosViaAcceso = {
    data: viaAcceso_datos, // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
    // Ahora debería haber tantos background colors como datos
    backgroundColor: [
        'rgba(163,221,203,0.2)',
        'rgba(232,233,161,0.2)',
        'rgba(230,181,102,0.2)',
    ],// Color de fondo
    borderColor: [
        'rgba(163,221,203,1)',
        'rgba(232,233,161,1)',
        'rgba(230,181,102,1)',
    ],// Color del borde
    borderWidth: 1,// Ancho del borde
};
// Función para crear gráficos de torta
function crearGrafico(canvasId, label, nombre,dataset) {
    var ctx = document.getElementById(canvasId).getContext('2d');
    return new Chart(ctx, {
        type: 'pie',
        data: {
            labels: label,
            datasets: [dataset]
        }
    });
}

// Crear gráficos de torta
var graficoMateria = crearGrafico('graficoMateria', materia_etiquetas,'Grafico de conteo de fallos por Materias',datosMateria);
var graficoOrigen = crearGrafico('graficoOrigen', origen_etiquetas,'Grafico de conteo de fallos por Origen',datosOrigen);
var graficoViaAcceso = crearGrafico('graficoViaAcceso', viaAcceso_etiquetas,'Grafico de conteo de fallos por Via de acceso',datosViaAcceso);
