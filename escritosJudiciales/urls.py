from django.urls import path

from . import views

app_name = 'escritosJudiciales'

urlpatterns = [
    path("", views.index, name="index"),
    path("clasificador/",views.clasificador,name="clasificador")
]