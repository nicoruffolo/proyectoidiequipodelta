from django.shortcuts import render
import os
from app.settings import BASE_DIR
import pdfplumber
from keras.models import load_model
import joblib
import numpy as np
from keras.preprocessing.sequence import pad_sequences
directorio = os.path.join(BASE_DIR)
carpeta_modelo = os.path.join(BASE_DIR,'escritosJudiciales', 'Modelo')
modelo_cargado = load_model(os.path.join(carpeta_modelo, 'ClasificadorDeCasos.h5'))
tokenizer = joblib.load(os.path.join(carpeta_modelo, 'tokenizer.pkl'))  
le = joblib.load(os.path.join(carpeta_modelo, 'label_encoder.pkl'))   
def index(request):
    listaDeArchivos = os.listdir(directorio)
    datos = listaDeArchivos[:5]
    return render(request, 'principal/listaDeArchivos.html', {"archivos": datos})
def porMateria(nombre,texto):
    def obtenerCategoria(texto):
        nomenc = texto[:3]
        return diccionarioMaterias.get(nomenc, None)
    diccionarioMaterias = {'CAF' : 'Contencioso Administrativo / tributario',
                        'CCF' : 'Civil y comercial',
                        'CIV' : 'Civil y comercial' ,
                        'CCC' : 'Penal y contravencional' ,
                        'CFP' : 'Penal y contravencional' ,
                        'CNT' : 'Laboral' ,
                        'COM' : 'Civil y comercial' ,
                        'CPE' : 'Penal y contravencional' ,
                        'CSS' : 'Previsional'}
    categoria = obtenerCategoria(nombre)
    if categoria:
        return categoria
    categorias_palabras_clave = {
    "Electoral": ["electoral"],
    "Civil y comercial": ["civil y comercial"],
    "Contencioso Administrativo / tributario": ["administrativo", "tributario", "afip", "organismos administración pública nacional"],
    "Previsional": ["previsional"],
    "Penal y contravencional": ["penal", "contravencional"],
    "Laboral": ["laboral"],
    "Familia": ["familia", "interés superior del niño", "convención los derecho del niño"],
    "Electoral": ["elecciones", "votación", "campaña"],
    "Civil y comercial": ["contrato", "compra", "venta", "propiedad"],
    "Contencioso Administrativo / tributario": ["impuestos"],
    "Previsional": ["anses", "jubilación", "pensión", "seguridad social"],
    "Penal y contravencional": ["delito", "crimen", "pena"],
    "Laboral": ["trabajo", "empleo", "despido"],
    "Familia": ["matrimonio", "divorcio"]
    }
    def identificar_categoria(texto):
    # Buscar coincidencias de palabras clave en el texto
        for categoria, palabras_clave in categorias_palabras_clave.items():
            for palabra_clave in palabras_clave:
                if esta_en_el_texto(texto, palabra_clave):
                #print(palabra_clave)
                    return categoria
                return None
    def esta_en_el_texto(texto, palabra_clave):
        return texto.find(palabra_clave) != -1
    categoria = identificar_categoria(texto)
    if categoria:
        return categoria
    
    secuencia_nuevo_texto = tokenizer.texts_to_sequences([texto])
    secuencia_nuevo_texto_padded = pad_sequences(secuencia_nuevo_texto, maxlen=3000) 
    prediccion = modelo_cargado.predict(secuencia_nuevo_texto_padded)
    etiqueta_predicha = le.inverse_transform([np.argmax(prediccion)])
    materia = etiqueta_predicha[0]
    return materia
def porOrigen(nombre,texto):
    def obtenerCategoria(texto):
        nomenc = texto[:3]
        return diccionarioMaterias.get(nomenc, None)

    #Algunos documentos pueden estar mal etiquetados
    diccionarioMaterias = {'FBB' : 'Federal',
                        'FCR' : 'Federal',
                        'FCB' : 'Federal',
                        'FCT' : 'Federal',
                        'FGR' : 'Federal',
                        'FLP' : 'Federal',
                        'FMP' : 'Federal',
                        'FMZ' : 'Federal',
                        'FPO' : 'Federal',
                        'FPA' : 'Federal',
                        'FRE' : 'Federal',
                        'FSA' : 'Federal',
                        'FRO' : 'Federal',
                        'FSM' : 'Federal',
                        'FTU' : 'Federal',
                        'CCF' : 'Federal',
                        'CAF' : 'Federal',
                        'CPF' : 'Federal',
                        'CSS' : 'Federal'
                         }
    categorias_palabras_clave = {
    "Federal": ["federal",
                "tratados internacionales",
                "constitución nacional",
                "ley federal",
                #organismos del estado
                "administración federal de ingresos públicos",
                "afip",
                "anses",
                "banco central de la república argentina (bcra)",
                "comisión nacional de valores (cnv)",
                "consejo profesional de ciencias económicas de la provincia de buenos aires",
                "dirección de inspección de personas jurídicas de la provincia de córdoba",
                "dirección de inspección de personas jurídicas de la provincia de entre ríos",
                "dirección de las personas jurídicas de la provincia de chaco",
                "dirección de personas jurídicas de la provincia de mendoza",
                "dirección de personas jurídicas de la provincia de tucumán",
                "dirección general de personas jurídicas de la provincia de río negro",
                "dirección general de superintendencia de personas jurídicas y registro público de comercio de la pampa créditos prendarios",
                "dirección nacional de migraciones",
                "dirección nacional del registro de la propiedad inmueble de la capital federal",
                "dirección provincial de personas jurídicas de la provincia de neuquén",
                "gendarmería nacional",
                "inspección general de justicia (i.g.j.)",
                "inspección general de justicia de la provincia de tierra del fuego",
                "inspección general de personas jurídicas de la provincia de catamarca",
                "inspección general de personas jurídicas de la provincia de formosa",
                "inspección general de personas jurídicas de la provincia de salta",
                "registro público de comercio de la provincia de salta",
                "ministerio de economía y finanzas públicas",
                "ministerio de relaciones exteriores, comercio internacional y culto",
                "oficina de constitución y fiscalización de personas jurídicas y cooperativas de la provincia de san luis",
                "policia federal argentina",
                "prefectura naval argentina",
                "registro nacional de las personas",
                "registro nacional de reincidencia",
                "registro público de comercio de la provincia de chaco",
                "registro público de comercio de la provincia de chubut",
                "registro público de comercio de la provincia de formosa",
                "registro público de comercio de la provincia de jujuy",
                "registro público de comercio de la provincia de santa fe",
                "registro público y archivo judicial de la provincia de mendoza",
                "secretaría de programación para la prevención de la drogadicción y la lucha contra el narcotráfico (sedronar)",
                "superintendencia de seguros de la nación",
                "ssn",
    ],
    "Provincial": ["provincial",
                   "suprema corte de justicia de la provincia de buenos aires",
                   "suprema corte de justicia de la provincia de mendoza",
                   "superior tribunal de justicia de la provincia de misiones",
                   "tribunal superior de justicia de la provincia de córdoba",
                   "corte de justicia de la provincia de san juan",
                   "corte suprema de justicia de la provincia de santa fe",
                   "superior tribunal de justicia de la provincia de entre ríos",
                   "superior tribunal de justicia de la provincia del chaco",
                   "superior tribunal de justicia de la provincia de la pampa",
                   "superior tribunal de justicia de la provincia de río negro",
                   "tribunal superior de justicia de catamarca",
                   "corte de justicia de catamarca",
                   "suprema corte de justicia de la provincia de jujuy",
                   "superior tribunal de justicia de formosa",
                   "tribunal superior de justicia de la provincia de santa cruz",
                   "superior tribunal de justicia de chubut",
                   "tribunal superior de justicia de la rioja",
                   "tribunal superior de justicia de la ciudad autónoma de buenos aires",
                   "superior tribunal de justicia de tierra del fuego e islas del atlántico sur",
                   "corte suprema de justicia de la provincia de tucumán",
                   "superior tribunal de justicia de santiago del estero",
                   "corte de justicia de salta",
                   "superior tribunal de justicia de la provincia de corrientes",
                   "tribunal superior de justicia de neuquén",
                   "superior tribunal de justicia de san luis",
                   "cámara nacional electoral o la cámara nacional en lo penal económico",
                   "justicia nacional",
                   "caba" ]
    }
    def esta_en_el_texto(texto, palabra_clave):
        return texto.find(palabra_clave) != -1
    def identificar_categoria(texto):
    # Buscar coincidencias de palabras clave en el texto
        for categoria, palabras_clave in categorias_palabras_clave.items():
            for palabra_clave in palabras_clave:
                if esta_en_el_texto(texto, palabra_clave):
                    return categoria
        return 'Provincial'
    categoria = obtenerCategoria(nombre)
    if categoria:
        return categoria
    return identificar_categoria(texto)
def porViaAcceso(nombre,texto):
    def esOriginario(texto):
        '''
        Este método devuelve true si el texto pertenece a un juicio originario
        precondición: Pasame el texto en minúsculas
        '''
        return texto.find("originario") != -1, "Originario"

    def esCompetencia(texto):
        '''
        Este método devuelve true si el texto pertenece a una competencia (pelea entre dos jueces)
        precondición: Pasame el texto en minúsculas
        '''
        return texto.find("competencia") != -1, "Competencia"
    if esOriginario(texto):
        return 'Originario'
    elif esCompetencia(texto):
        return 'Competencia'
    return "Recurso"
def obtener_texto(ubicacionPDF):
    # Crear el PDF a leer
    pdf = pdfplumber.open(ubicacionPDF)
    texto = ""
    # Pagina a pagina formar el texto completo
    for page in pdf.pages:
        texto += page.extract_text()
    # Cerrar PDF
    pdf.close()
    return texto
def obtenerExpedientesYCausas(texto):
    matcher = Matcher(nlp.vocab)
    matcherdo = Matcher(nlp.vocab)
    doc = nlp(texto)

    matcher.add("intento",[
        [{"lower":"autos"}]
        ])
    matcherdo.add("intento",[
        [{"lower":"considerando"}]
        ])
    coin = matcher(doc)
    coindo = matcherdo(doc)
    if (not(coin and coindo)):
        resultado = {
        "Expedientes": 1,
        "Causas": 1
        }
        return resultado
def clasificador(request):
    directorio = os.path.join(BASE_DIR)
    listaDeArchivos = os.listdir(directorio)
    ok = True
    conteo = {
    "Civil_y_comercial":0,
    "Contencioso_Administrativo_tributario":0,
    "Contencioso_Administrativo__tributario":0,
    "Familia":0,
    "Electoral":0,
    "Previsional":0,
    "Penal_y_contravencional":0,
    "Laboral":0,
    "Otros":0,
    "Provincial":0,
    "Federal":0,
    "Competencia":0,
    "Recurso":0,
    "Originario":0
    }
    resultados=[]
    for archivo in listaDeArchivos:
        rutaDelArchivo = os.path.join(directorio,archivo)
        texto_completo = obtener_texto(rutaDelArchivo)
        if ok:
            print(texto_completo)
            ok = False
        materia = ((porMateria(archivo,texto_completo)).replace("/","")).replace(" ","_")
        origen = porOrigen(archivo,texto_completo)
        viaDeAcceso = porViaAcceso(archivo,texto_completo)
        conteo[materia] = conteo[materia] + 1
        conteo[origen] = conteo[origen] + 1
        conteo[viaDeAcceso] = conteo[viaDeAcceso] + 1
        resultados.append( (archivo,materia,origen,viaDeAcceso) ) 

    materia_conteo = {
        "Civil_y_comercial": conteo["Civil_y_comercial"],
        "Contencioso_Administrativo_tributario": (conteo["Contencioso_Administrativo_tributario"] + conteo["Contencioso_Administrativo__tributario"]),
        "Familia": conteo["Familia"],
        "Electoral": conteo["Electoral"],
        "Previsional": conteo["Previsional"],
        "Penal_y_contravencional": conteo["Penal_y_contravencional"],
        "Laboral": conteo["Laboral"],
        "Otros": conteo["Otros"]
    }

    # Subdiccionario para la categoría "Origen"
    origen_conteo = {
        "Provincial": conteo["Provincial"],
        "Federal": conteo["Federal"]
    }

    # Subdiccionario para la categoría "Vía de Acceso"
    viaAcceso_conteo = {
        "Competencia": conteo["Competencia"],
        "Recurso": conteo["Recurso"],
        "Originario": conteo["Originario"]
    }
    return render(request, 'principal/resultados.html', {"materia": materia_conteo,"origen":origen_conteo,"viaAcceso":viaAcceso_conteo})