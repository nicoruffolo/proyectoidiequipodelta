from django.urls import path
from . import views

app_name = 'requerimientos'

urlpatterns = [
    path("", views.index, name="index"),
    path("compare_text/", views.compare_texts, name="compare_text"),
    path('download/', views.download, name='download'),
]