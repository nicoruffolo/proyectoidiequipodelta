from django.shortcuts import render
import os
import tempfile
import spacy
from django.http import JsonResponse, HttpResponse
from sentence_transformers import SentenceTransformer, util
from django.urls import reverse
from spacy.tokens import Token

from requerimientos.src import Lesk, PDF_Creator
from requerimientos.src.Other_functions import calculate_semantic_similarity, count_adjectives_and_verbs, expand_with_synonyms, normalize_text, calculate_word_similarity, calculate_negation_impact, get_semantic_description, evaluate_detail_richness

# Inicializar SpaCy y otros componentes necesarios
nlp = spacy.load("es_core_news_lg")
model = SentenceTransformer('paraphrase-multilingual-MiniLM-L12-v2')

# Define custom extensions
Token.set_extension('synonyms', default=None, force=True)
Token.set_extension('antonyms', default=None, force=True)

def index(request):
    return render(request, 'index_requerimientos.html')

# Función principal para comparar textos
def compare_texts(request):
    if request.method == 'POST':
        text1 = request.POST.get('text1').lower()
        text2 = request.POST.get('text2').lower()

        doc1 = nlp(text1)
        doc2 = nlp(text2)

        # Aplicar Lesk para obtener sentidos de las palabras
        doc1_senses = Lesk.apply_lesk(doc1)
        doc2_senses = Lesk.apply_lesk(doc2)

        semantic_similarity_score = calculate_semantic_similarity(doc1_senses, doc2_senses)

        # Contar adjetivos y verbos
        adjectives1, verbs1 = count_adjectives_and_verbs(doc1)
        adjectives2, verbs2 = count_adjectives_and_verbs(doc2)

        # Expandir textos con sinónimos y normalizar
        expanded_text1 = nlp(expand_with_synonyms(doc1))
        expanded_text2 = nlp(expand_with_synonyms(doc2))
        reconstructed_text1 = normalize_text(expanded_text1)
        reconstructed_text2 = normalize_text(expanded_text2)

        # Calcular la similaridad estructural y semántica
        embeddings = model.encode([reconstructed_text1, reconstructed_text2])
        structural_similarity = util.pytorch_cos_sim(embeddings[0], embeddings[1]).item()

        word_similarity = calculate_word_similarity(doc1, doc2)
        negation_impact = calculate_negation_impact(doc1, doc2)
        adjusted_similarity = ((structural_similarity * negation_impact) + word_similarity) / 2

        print(adjusted_similarity)

        semantic_description = get_semantic_description(semantic_similarity_score)


        detail_richness_result = evaluate_detail_richness(adjectives1, adjectives2, verbs1, verbs2)

        print(semantic_description)

        pdf_path = PDF_Creator.create_pdf(adjusted_similarity, semantic_similarity_score, word_similarity, text1, text2, semantic_description, detail_richness_result)

        pdf_url = request.build_absolute_uri(reverse('requerimientos:download') + f'?filename={os.path.basename(pdf_path)}') if pdf_path else None

        result = {
            "similarity": adjusted_similarity,
            "semantic_similarity_lesk":semantic_similarity_score,
            "word_similarity": word_similarity,
            "negations_text1": [token.text for token in doc1 if token.lemma_ in {"no", "nunca", "tampoco", "ninguna", "jamás"}],
            "negations_text2": [token.text for token in doc2 if token.lemma_ in {"no", "nunca", "tampoco", "ninguna", "jamás"}],
            "pdf_url": pdf_url
        }

        return JsonResponse(result)

# Función para descargar el PDF
def download(request):
    filename = request.GET.get('filename')
    if not filename:
        return HttpResponse(status=400, content="Filename not provided")

    file_path = os.path.join(tempfile.gettempdir(), filename)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as f:
            response = HttpResponse(f.read(), content_type='application/pdf')
            response['Content-Disposition'] = f'attachment; filename="{filename}"'
            return response
    else:
        return HttpResponse(status=404)
