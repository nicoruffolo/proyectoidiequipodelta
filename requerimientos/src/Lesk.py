from nltk.corpus import wordnet as wn
from googletrans import Translator
from pywsd.lesk import simple_lesk

translator = Translator()

def get_wordnet_pos(spacy_tag):
    """
    Convierte etiquetas de partes del discurso (POS) de spaCy a etiquetas compatibles con WordNet.

    Args:
        spacy_tag (str): Etiqueta POS de spaCy de un token.

    Returns:
        str: Etiqueta de POS de WordNet correspondiente o None si no hay equivalente.
    """
    if spacy_tag.startswith('N'):
        return wn.NOUN
    elif spacy_tag.startswith('V'):
        return wn.VERB
    elif spacy_tag.startswith('J'):
        return wn.ADJ
    elif spacy_tag.startswith('R'):
        return wn.ADV
    return None

def apply_lesk(doc):
    """
    Aplica el algoritmo de Lesk modificado a cada token en un documento spaCy para determinar el sentido de las palabras
    basado en su contexto. El algoritmo de Lesk utiliza una ventana de contexto alrededor de cada palabra para mejorar
    la precisión de la desambiguación.

    Args:
        doc (spacy.tokens.Doc): Documento procesado por spaCy.

    Returns:
        list of tuples: Lista de tuplas donde cada tupla contiene el texto del token y la definición del sentido más probable
        o 'No definition found' si no se encuentra ningún sentido.
    """
    senses = []
    for token in doc:
        wn_pos = get_wordnet_pos(token.pos_)
        if wn_pos:
            start = max(token.i - 5, 0)
            end = min(token.i + 5, len(doc))
            context = doc[start:end].text
            translated_word = translator.translate(token.text, src='es', dest='en').text
            translated_context = translator.translate(context, src='es', dest='en').text
            print(f"Token: {token.text}, Translated Token: {translated_word}, Context: {context}, Translated Context: {translated_context}, POS: {wn_pos}")
            sense = simple_lesk(translated_context, translated_word, pos=wn_pos)
            if sense:
                print(f"Sense found for {token.text}: {sense.definition()}")
                senses.append((token.text, sense.definition()))
            else:
                print(f"No definition found for {token.text}")
                senses.append((token.text, 'No definition found'))
    return senses