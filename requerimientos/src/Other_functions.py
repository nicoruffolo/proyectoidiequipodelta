from spacy.language import Language

def normalize_text(doc):
    """
    Reconstruye el texto de un documento spaCy normalizando las oraciones y reemplazando pronombres con sujetos explícitos.

    Args:
        doc (Doc): Un objeto `Doc` de spaCy que contiene el texto a normalizar.

    Returns:
        str: Texto normalizado con sujetos explícitos en lugar de pronombres.
    """

    reconstructed_text = []
    last_subject = None
    for sent in doc.sents:
        new_sentence = []
        subject_in_sentence = False
        for token in sent:
            if token.dep_ == 'nsubj':
                if token.pos_ != 'PRON':
                    last_subject = token.text
                    subject_in_sentence = True
                new_sentence.append(token.text)
            elif token.dep_ == 'nsubj' and token.pos_ == 'PRON':
                new_sentence.append(last_subject if last_subject else token.text)
                subject_in_sentence = True
            else:
                new_sentence.append(token.text)
                
        if not subject_in_sentence and last_subject:
            reconstructed_text.append(last_subject + " " + " ".join(new_sentence))
        else:
            reconstructed_text.append(" ".join(new_sentence))
    return " ".join(reconstructed_text)


def expand_with_synonyms(doc):
    # Crea una lista para almacenar el nuevo texto con sinónimos
    new_tokens = []
    for token in doc:
        new_tokens.append(token.text)  # Añade el texto original
        if token._.synonyms:
            # Añade el primer sinónimo al texto, solo si la lista no está vacía
            new_tokens.append(token._.synonyms[0] if len(token._.synonyms) > 0 else token.text)
    # Reconstruye el texto expandido
    expanded_text = ' '.join(new_tokens)
    # Retorna un nuevo documento procesado
    return expanded_text

def count_adjectives_and_verbs(doc):
    """
    Cuenta la cantidad de adjetivos y verbos en un documento de spaCy.
    
    Args:
        doc (Doc): Un objeto `Doc` de spaCy.
    
    Returns: 
        tuple: Una tupla con la cantidad de adjetivos y verbos en el documento.
    """
    adjectives = {token.lemma_ for token in doc if token.pos_ == 'ADJ'}
    verbs = {token.lemma_ for token in doc if token.pos_ == 'VERB'}
    return len(adjectives), len(verbs)

def calculate_word_similarity(doc1, doc2):
    """
    Calcula la similitud entre dos documentos basada en la intersección de palabras importantes.

    Args:
        doc1 (spacy.tokens.Doc): El primer documento a comparar.
        doc2 (spacy.tokens.Doc): El segundo documento a comparar.

    Returns:
        float: Un puntaje de similitud entre 0 y 1, donde 1 indica que las palabras importantes 
               en ambos documentos son idénticas.
    """
    important_words1 = {token.lemma_ for token in doc1 if not token.is_stop and not token.is_punct}
    important_words2 = {token.lemma_ for token in doc2 if not token.is_stop and not token.is_punct}
    return len(important_words1.intersection(important_words2)) / max(len(important_words1), len(important_words2))

def calculate_negation_impact(doc1, doc2):
    """
    Calcula el impacto de las palabras de negación en dos documentos.

    Args:
        doc1 (spacy.tokens.Doc): El primer documento a analizar.
        doc2 (spacy.tokens.Doc): El segundo documento a analizar.

    Returns:
        float: Un valor de 0.5 si se encuentra alguna palabra de negación, o 1.0 si no se encuentran palabras de negación.
    """
    negation_words = {"no", "nunca", "tampoco", "ninguna", "jamás"}
    negations_text1 = [token for token in doc1 if token.lemma_ in negation_words]
    negations_text2 = [token for token in doc2 if token.lemma_ in negation_words]
    return 0.5 if negations_text1 or negations_text2 else 1.0

def get_semantic_description(similarity):
    """
    Devuelve una descripción semántica basada en la similitud entre textos.

    Args: 
        similarity (float): Un valor de similitud entre 0 y 1.

    Returns: 
        str: Descripción semántica basada en el valor de similitud.
"""
    
    if similarity > 0.90:
        return "Los textos significan lo mismo"
    elif similarity > 0.85:
        return "Textos muy similares"
    elif similarity > 0.75:
        return "Textos similares"
    elif similarity > 0.60:
        return "Textos medianamente similares"
    elif similarity > 0.45:
        return "Textos con algunas diferencias semanticas"
    elif similarity > 0.20:
        return "Textos semanticamente distintos"
    else:
        return "Textos semanticamente muy distintos"

def evaluate_detail_richness(adj_count1, adj_count2, verb_count1, verb_count2):
    """
    Evalúa la riqueza en detalles de dos textos considerando la cantidad de adjetivos y verbos.

    Args:
        adj_count1 (int): Cantidad de adjetivos en el primer texto.
        adj_count2 (int): Cantidad de adjetivos en el segundo texto.
        verb_count1 (int): Cantidad de verbos en el primer texto.
        verb_count2 (int): Cantidad de verbos en el segundo texto.

    Returns:
        str: Descripción comparativa de la riqueza en detalles de ambos textos.
    """
    richness1 = adj_count1 + verb_count1
    richness2 = adj_count2 + verb_count2
    if richness1 > richness2:
        return "Texto 1 es más rico en detalles."
    elif richness2 > richness1:
        return "Texto 2 es más rico en detalles."
    else:
        return "Ambos textos tienen una riqueza en detalles similar."

def calculate_semantic_similarity(senses1, senses2):
    """
    Calcula la similitud semántica entre dos conjuntos de sentidos, utilizando una métrica basada en la intersección
    de definiciones comunes ajustada por el total de definiciones únicas en ambos conjuntos.

    Args:
        senses1 (list): Lista de sentidos del primer texto, donde cada sentido es una tupla (palabra, definición).
        senses2 (list): Lista de sentidos del segundo texto, de forma similar al primer argumento.

    Returns:
        float: Porcentaje de similitud semántica entre los dos conjuntos de sentidos, donde un valor más alto
        indica mayor similitud.
    """
    common_senses = 0
    total_senses = len(senses1) + len(senses2)

    # Crear diccionarios para contar las ocurrencias de cada definición en ambos conjuntos de sentidos
    sense_count1 = {}
    sense_count2 = {}

    print("Senses1:", senses1)  # Print del primer conjunto de sentidos
    print("Senses2:", senses2)  # Print del segundo conjunto de sentidos

    for word, sense in senses1:
        if sense not in sense_count1:
            sense_count1[sense] = 0
        sense_count1[sense] += 1

    for word, sense in senses2:
        if sense not in sense_count2:
            sense_count2[sense] = 0
        sense_count2[sense] += 1

    # Contar sentidos comunes
    for sense in sense_count1:
        if sense in sense_count2:
            common_senses += min(sense_count1[sense], sense_count2[sense])

    print("Common senses count:", common_senses)  # Print del conteo de sentidos comunes

    if total_senses > 0:
        # El porcentaje de similitud es el número de sentidos comunes dividido por el total de sentidos únicos
        similarity = (2 * common_senses / total_senses)
        print(f"Semantic similarity: {similarity}%")  # Print del porcentaje de similitud semántica
        return similarity
    else:
        return 0