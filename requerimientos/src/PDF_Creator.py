from django.conf import settings
from fpdf import FPDF
import os
import uuid
import tempfile

def create_pdf(adjusted_similarity, semantic_similarity_score, word_similarity, text1, text2, semantic_description, detail_richness_result):
    pdf = FPDF()
    pdf.add_page()
    pdf.set_margins(15, 10, 15)  # Establece los márgenes (izquierda, arriba, derecha)

    logo_path = os.path.join(settings.BASE_DIR, 'requerimientos', 'Static', 'delta.png')
    pdf.image(logo_path, x=170, y=10, w=25)  # Ajusta la posición (x, y) y el tamaño (w - ancho)
    
    pdf.ln(10)  # Añade un espacio vertical

    pdf.set_font("Arial", 'B', 12)
    pdf.cell(0, 10, 'Resultados de Comparación de Textos', 0, 1, 'C')

    pdf.set_font("Arial", 'I', 10)
    pdf.multi_cell(0, 9, 'Descripción semántica: ' + semantic_description, 0, 'C')
    
    pdf.set_font("Arial", 'I', 10)
    pdf.multi_cell(0, 9, 'Análisis de Riqueza en Detalles: ' + detail_richness_result, 0, 'C')
    pdf.ln(5)  # Añade un espacio vertical más grande al final

    pdf.set_font("Arial", 'B', 10)
    pdf.cell(55, 10, 'Similitud estructural:', 1, 0, 'C')
    pdf.set_font("Arial", size=10)
    pdf.cell(0, 10, f'{adjusted_similarity:.2f}', 1, 1, 'C')

    pdf.set_font("Arial", 'B', 10)
    pdf.cell(55, 10, 'Similitud semántica (Lesk):', 1, 0, 'C')
    pdf.set_font("Arial", size=10)
    pdf.cell(0, 10, f'{semantic_similarity_score:.2f}', 1, 1, 'C')

    pdf.set_font("Arial", 'B', 10)
    pdf.cell(55, 10, 'Similitud en palabras:', 1, 0, 'C')
    pdf.set_font("Arial", size=10)
    pdf.cell(0, 10, f'{word_similarity:.2f}', 1, 1, 'C')

    pdf.ln(5)  # Añade un espacio vertical

    pdf.set_font("Arial", 'B', 10)
    pdf.cell(0, 9, 'Texto 1:', 0, 1, 'C')
    pdf.set_font("Arial", size=10)
    pdf.multi_cell(0, 9, text1, 0, 'C')
    pdf.ln(3)  # Añade un espacio vertical

    pdf.set_font("Arial", 'B', 10)
    pdf.cell(0, 9, 'Texto 2:', 0, 1, 'C')
    pdf.set_font("Arial", size=10)
    pdf.multi_cell(0, 9, text2, 0, 'C')
    pdf.ln(5)  # Añade un espacio vertical

    pdf_filename = f'compare_result_{uuid.uuid4()}.pdf'
    pdf_path = os.path.join(tempfile.gettempdir(), pdf_filename)
    pdf.output(pdf_path)
    return pdf_path